// @flow
import "./styles/global.scss";

import "babel-polyfill";
import "url-search-params-polyfill";

import React from "react";
import {render} from "react-dom";
import {AppContainer} from "react-hot-loader";

import {createStore, combineReducers, applyMiddleware} from "redux";

import {Route} from "react-router";
import {ConnectedRouter, routerReducer, routerMiddleware} from "react-router-redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import reducers from "./rootReducer";
import createHistory from "history/createHashHistory";

import App from "./app/components/App";

const history = createHistory();
const store = createStore(
    combineReducers({
        ...reducers,
        routing: routerReducer
    }),
    String(process.env) !== "production" ? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() : {},
    applyMiddleware(thunk, routerMiddleware(history))
);

const root = document.getElementById("root");

if (root) {
    render(
        <Provider store={store}>
            <AppContainer>
                <ConnectedRouter history={history}>
                    <Route path="/" component={App}/>
                </ConnectedRouter>
            </AppContainer>
        </Provider>,
        root
    );
}

if (module.hot) {
    module.hot.accept("./rootReducer", () => {
        const nextRootReducer = require("rootReducer");
        store.replaceReducer(nextRootReducer);
    });

    module.hot.dispose(data => {
        data.store = store;
    });
}

if (navigator.serviceWorker) {
    window.addEventListener("load", () => {
        // $FlowFixMe
        navigator.serviceWorker.register("/service-worker.js")
            .then(registration => {
                console.log("SW registered: ", registration);
                if (process.env.NODE_ENV === "development") {
                    console.log("Make sure you have 'Upload on reload' checked (Chrome Dev Tools -> Application -> Service Workers) for correct hot reload");
                }
            })
            .catch(error => console.log("SW registration failed: ", error));
    });
}
