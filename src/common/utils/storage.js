//@flow
import config from "config";
import Cookies from "js-cookie";
import bowser from "bowser";

const defaultStorageProvider = window.localStorage;

class CookieStorageProvider {
    cookiePrefix = config.COOKIE_STORAGE_PREFIX;
    storedKeys = new Set();

    setItem(key: string, content: any) {
        this.storedKeys.add(key);
        Cookies.set(this.cookiePrefix + key, content);
    }

    getItem(key: string): any {
        return Cookies.get(this.cookiePrefix + key);
    }

    removeItem(key: string) {
        this.storedKeys.delete(key);
        Cookies.remove(this.cookiePrefix + key);
    }

    clear() {
        this.storedKeys.forEach(key => Cookies.remove(this.cookiePrefix + key));
    }
}

const Storage = () => {
    const storageProvider = bowser.safari ? new CookieStorageProvider() : defaultStorageProvider;

    const setItem = (key: string, content: any): void => {
        storageProvider.setItem(key, JSON.stringify(content));
    };

    const getItem = (key: string): any => {
        try {
            return JSON.parse(storageProvider.getItem(key));
        } catch (error) {
            return null;
        }
    };

    const removeItem = (key: string): void => {
        storageProvider.removeItem(key);
    };

    const pushItem = (key: string, content: any): void => {
        const arrayItem = getItem(key) || [];
        arrayItem.push(content);
        setItem(key, arrayItem);
    };

    const clear = (): void => {
        storageProvider.clear();
    };

    return {
        setItem,
        getItem,
        pushItem,
        removeItem,
        clear
    };
};

export default Storage();
