// @flow
import * as React from "react";
import isEqual from "lodash/isEqual";

type Props = {
    location?: Object
};

export default (propFunctionNames: string[]) => (Component: React.ComponentType<any>) => class OnLocationChange extends React.Component<Props> {
    props: Props;

    componentWillReceiveProps(nextProps: Props) {
        if (!isEqual(this.props.location, nextProps.location)) {
            for (let functionName of propFunctionNames) {
                if (typeof nextProps[functionName] === "function") {
                    nextProps[functionName]();
                }
            }
        }
    }

    render() {
        return <Component {...this.props}/>;
    }
};
