// @flow
import {LazyComponent} from "common/components";

export default LazyComponent(() => import("./ErrorMessageOverlayContainer" /* webpackChunkName: "error" */), {showLoading: false});
