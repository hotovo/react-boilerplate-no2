// @flow
import * as React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {clearErrors} from "app/actions";
import {getErrors} from "app/selectors";

import ErrorMessageOverlay from "./ErrorMessageOverlay";
import {OnLocationChange} from "common/hoc";

type Props = {
    errors: string[],
    clearErrors: () => void
};

@OnLocationChange([])
class ErrorMessageOverlayContainer extends React.Component<Props> {
    props: Props;

    render() {
        const {errors, clearErrors} = this.props;

        return (
            <React.Fragment>
                {errors.length > 0 && <ErrorMessageOverlay messages={errors} done={clearErrors}/>}
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    ...ownProps,
    errors: getErrors(state)
});
const mapDispatchToProps = dispatch => bindActionCreators({
    clearErrors
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ErrorMessageOverlayContainer);
