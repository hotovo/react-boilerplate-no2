// @flow
import {LazyComponent} from "common/components";

export default LazyComponent(() => import("./AppContainer" /* webpackChunkName: "app" */), {className: "expand"});
