// @flow
import React from "react";
import {Route} from "react-router";

import {ErrorMessageOverlay} from "app/components";
import App from "./App";

import "./app.scss";

class AppContainer extends React.Component<*> {
    render() {
        return (
            <App>
                <Route path="/" component={ErrorMessageOverlay}/>
            </App>
        );
    }
}

export default AppContainer;
