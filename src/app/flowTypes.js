// @flow
export type State = {
    errors: string[],
    previousPath: ?string,
    currentPath: ?string
};

export type StoreState = {
    app: State
};
