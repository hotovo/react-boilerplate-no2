// @flow
import {LOCATION_CHANGE} from "react-router-redux";
import reduceReducers from "reduce-reducers";

import * as types from "app/actionTypes";
import type {State} from "app/flowTypes";

export const initialState: State = {
    currentPath: null,
    previousPath: null,
    errors: []
};

export default reduceReducers(
    (state: State = initialState, action: Object) => {
        switch (action.type) {
            case LOCATION_CHANGE: {
                return {
                    ...state,
                    previousPath: state.currentPath === action.payload.pathname ? state.previousPath : state.currentPath,
                    currentPath: action.payload.pathname
                };
            }
            case types.CLEAR_PATHS: {
                return {
                    ...state,
                    previousPath: null,
                    currentPath: null
                };
            }
            case types.SET_ERRORS: {
                return {
                    ...state,
                    errors: action.errors
                };
            }
            case types.CLEAR_ERRORS: {
                return {
                    ...state,
                    errors: []
                };
            }

            default: {
                return state;
            }
        }
    }
);
